<?php
    include 'koneksi.php';

    $nama = $_POST['nama'];
    $email = $_POST['email'];
    $website = $_POST['website'];
    $kelas = $_POST['kelas'];
    $alamat = $_POST['alamat'];

    $query = "INSERT INTO data (no, nama, email, website, kelas, alamat) VALUES (null, '$nama', '$email', '$website', '$kelas', '$alamat')";
    if (!mysqli_query($connection, $query)){
        echo 'not inserted';
    }
?>


<!DOCTYPE html>
<html lang="en">    
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Biodata</title>
    <link rel="shorcut icon" href="img/favicon.png">
    <link rel="stylesheet" href="css/bootstrap.min.css">
<!--    <link rel="stylesheet" href="css/style.css">-->
    <link rel="stylesheet" href="css/responsive.css">
</head>
<body>
    <div class="tabel">
        <table class="table">
        <thead class="thead-dark">
            <tr>
              <th scope="col">No</th>
              <th scope="col">Nama</th>
              <th scope="col">Email</th>
              <th scope="col">Website</th>
              <th scope="col">Kelas</th>
              <th scope="col">Alamat</th>
            </tr>
        </thead>
        <?php
            $ambil = mysqli_query($connection, "SELECT * FROM data");
            while ($data = mysqli_fetch_assoc($ambil)){
        ?>
        <tbody>
            <tr>
              <th scope="col"><?php echo $data['no']?></th>
              <th scope="col"><?php echo $data['nama']?></th>
              <th scope="col"><?php echo $data['email']?></th>
              <th scope="col"><?php echo $data['website']?></th>
              <th scope="col"><?php echo $data['kelas']?></th>
              <th scope="col"><?php echo $data['alamat']    ?></th>
        </tbody>
        <?php
        }
        ?>
    </table>
    <a class="btn btn-warning col-sm-2" href="index.php" role="button" style="color: white">Back</a>

    </div>
        
    
    
    <script src="js/bootstrap.min.js" ></script>
</body>

</html>
