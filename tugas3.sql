-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 20 Nov 2018 pada 02.00
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tugas3`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data`
--

CREATE TABLE `data` (
  `no` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(60) NOT NULL,
  `website` varchar(50) NOT NULL,
  `kelas` varchar(25) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data`
--

INSERT INTO `data` (`no`, `nama`, `email`, `website`, `kelas`, `alamat`) VALUES
(1, 'Aidia Oemami', 'aidiaoemami@gmail.com', 'aidiaoemami.com', 'PPAW-H', 'Permai 5'),
(2, 'Aidia', 'oemamiaidia@gmail.com', 'aidiaoemami.com', 'PPAW-H', 'Permai 5'),
(3, 'Bunga', 'bunga32@gmail.com', 'bunga.co.id', 'PPAW-A', 'Bandung'),
(4, 'Bayu', 'bayu43@gmail.com', 'bayubay.com', 'PPAW-B', 'Cibiru Hilir'),
(5, 'Dika', 'dika55@gmail.com', 'dikadik.com', 'PPAW-F', 'Cinunuk'),
(6, 'Erni', 'erni67@gmail.com', 'eerni.com', 'PPAW-C', 'Cilengkrang'),
(7, 'Firman', 'firmanf@gmail.com', 'firm.com', 'PPAW-G', 'Cileunyi'),
(8, 'Gina', 'gingin@gmail.com', 'ginaaa.com', 'PPAW-D', 'Buah Batu');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data`
--
ALTER TABLE `data`
  ADD PRIMARY KEY (`no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data`
--
ALTER TABLE `data`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
